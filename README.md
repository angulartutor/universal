# Angular SSR
> SSR: Server side rendering


Angular SSR, also known as Universal or Isomorphic, is a mechanism for serving the same content from multiple platforms (typically "client" and "server").
That is, you can deploy your app either to client or to server and serve the same content.

In practice, since Angular originally started as a client-side-only app,
Angular Universal really means adding the server support (for SEO/performance reasons, etc.) to a client-side app. 
That is, the server-side rendered app will not be purely server-side only (as in static HTML and CSS files only), strictly speaking. The server will also serve the client app (the traditional Angular client-side app) in addition to the server-side rendered static content.

Angular Universal has been being developed for a while,
but its support has been finally added in CLI version 1.6.


## How to Enable Angular SSR

_(Note: Angular Universal support was officially added in Angular CLI 1.6. Hence you'll need to upgrade your CLI first if you want to use CLI for enabling SSR.)_


    ng g universal <server-app-name>

The server-side app name is used by CLI (to distinguish it from the client-side app, which by default has no name).
You can build the server app as follows:

    ng build --app <server-app-name>


As of this writing, you cannot run the server app using CLI.
You'll need to create a Web server (e.g., using Express) to serve your SSR server app.


### Update

It appears that, since the last time I used `ng generate universal`, there have been some changes in CLI command syntax. 
As of Angular CLI v7.2.3, I could not successfully run `ng g universal` (I kept getting some weired errors).

One alternative way to generate SSR in Angular 7 is to use the following schematics:

    ng add @nguniversal/express-engine --clientProject my-app

This command generates an "app shell" as well for the server-side app (in Express),
which I presume is a separate step if you use `ng g`.



_tbd_


